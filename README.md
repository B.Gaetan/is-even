# is-even

Simple, clean and ready to use code to check if a given number is even.

## Why

This project was created to help new .NET programmers, because as you know, sometimes it could be usefull to check if a given number is even or not, without headaches.

## How to use it

First, clone this repo or simply download the IsEvenHelper.
Then you can use it like:

```csharp
IsEvenHelper.IsEven(0); // true;
IsEvenHelper.IsEven(1); // false;
IsEvenHelper.IsEven(2); // true;
IsEvenHelper.IsEven(3); // false;
// and so on, simple as that!
```

Feel free to make a package of it for better reuse! :)

## Disclaimer

This is not my original idea but simply a C# implementation of is-even, you can found others similar projects for different languages on the web.